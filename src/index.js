'use strict';

import Backbone from 'backbone';
import * as CONSTANTS from 'constants';
import * as Company from 'models/company';

Backbone.$.ajaxSetup({
	// X-Requested-With is not allowed by Access-Control-Allow-Headers
	crossDomain: true,
	dataType: 'json',
	contentType: 'application/json',
	xhrFields: { withCredentials: true },
	headers: { 'X-Suppress-HTTP-Code': true },
	beforeSend: (xhr, options) => {
		options.url = [CONSTANTS.BASE_API_URL, options.url].join('/');
	}
});

let companyModel = new Company.CompanyModel({ id: 2776 });

companyModel.fetch()
.then((response) => {
	console.log('Company response', response)
});

export default class REST {
	constructor() {
		this._name = 'REST';
	}

	get name() {
		return this._name;
	}
}