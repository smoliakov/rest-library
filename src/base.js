'use strict';

import Backbone from 'backbone';

export class BaseModel extends Backbone.Model {
	fetch(query = {}, options = {}) {
		// set query as data for Backbone fetch method
		if (Object.keys(query).length) options.data = query;

		// call super fetch with needed options
		return super.fetch(options);
	}

	sync(method, model, ...options) {
		console.log(method, model, options)

		return super.sync(...arguments);
	}
}

export class BaseCollection extends Backbone.Collection {

}