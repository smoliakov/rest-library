'use strict';

import { BaseModel, BaseCollection } from '../base';

export class CompanyModel extends BaseModel {
	constructor() {
		super(...arguments);
		this.urlRoot = 'company';
	}

	defaults() {
		return {
			name: '',
			alias: '',
			image_url: ''
		}
	}
}

export class CompanyCollection extends BaseCollection {

}