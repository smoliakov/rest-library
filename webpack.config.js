const webpack = require('webpack');
const UglifyJsPlugin = webpack.optimize.UglifyJsPlugin;
const path = require('path');
const env = require('yargs').argv.mode;

let plugins = [];
let outputFile = 'rest.js';

if (env === 'build') {
	plugins.push(new UglifyJsPlugin({ minimize: true }));
	outputFile = 'rest.min.js';
}

var config = {
	entry: path.join(__dirname, '/src/index.js'),
	devtool: 'source-map',
	output: {
		path: path.join(__dirname, '/lib'),
		filename: outputFile,
		library: 'REST',
		libraryTarget: 'umd',
		umdNamedDefine: true
	},
	module: {
		loaders: [
			{
				test: /(\.jsx|\.js)$/,
				loader: 'babel',
				exclude: /(node_modules|bower_components)/
			}
			// ,
			// {
			// 	test: /(\.jsx|\.js)$/,
			// 	loader: "eslint-loader",
			// 	exclude: /node_modules/
			// }
		]
	},
	resolve: {
		root: path.resolve('./src'),
		extensions: ['', '.js']
	},
	externals: {
		jquery: 'jQuery',
		lodash: {
			commonjs: 'lodash',
			commonjs2: 'lodash',
			amd: '_',
			root: '_'
		},
		backbone: 'Backbone'
	},
	plugins: plugins
};

module.exports = config;
